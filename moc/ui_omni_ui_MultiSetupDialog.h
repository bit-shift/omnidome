/********************************************************************************
** Form generated from reading UI file 'omni_ui_MultiSetupDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OMNI_UI_MULTISETUPDIALOG_H
#define UI_OMNI_UI_MULTISETUPDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "proj/MultiSetupParameters.h"
#include "proj/MultiSetupPreview.h"

namespace omni {
namespace ui {
namespace proj {

class Ui_MultiSetupDialog
{
public:
    QVBoxLayout *verticalLayout;
    QGroupBox *groupSetup;
    QHBoxLayout *horizontalLayout_2;
    QSplitter *splitter;
    omni::ui::proj::MultiSetupParameters *parameters;
    omni::ui::proj::MultiSetupPreview *preview;
    QWidget *boxButtons;
    QHBoxLayout *horizontalLayout;
    QToolButton *btnCancel;
    QSpacerItem *horizontalSpacer;
    QToolButton *btnReplace;
    QToolButton *btnAppend;

    void setupUi(QDialog *omni__ui__proj__MultiSetupDialog)
    {
        if (omni__ui__proj__MultiSetupDialog->objectName().isEmpty())
            omni__ui__proj__MultiSetupDialog->setObjectName(QStringLiteral("omni__ui__proj__MultiSetupDialog"));
        omni__ui__proj__MultiSetupDialog->resize(927, 601);
        omni__ui__proj__MultiSetupDialog->setModal(true);
        verticalLayout = new QVBoxLayout(omni__ui__proj__MultiSetupDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(5, 5, 5, 5);
        groupSetup = new QGroupBox(omni__ui__proj__MultiSetupDialog);
        groupSetup->setObjectName(QStringLiteral("groupSetup"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(groupSetup->sizePolicy().hasHeightForWidth());
        groupSetup->setSizePolicy(sizePolicy);
        horizontalLayout_2 = new QHBoxLayout(groupSetup);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        splitter = new QSplitter(groupSetup);
        splitter->setObjectName(QStringLiteral("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        parameters = new omni::ui::proj::MultiSetupParameters(splitter);
        parameters->setObjectName(QStringLiteral("parameters"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(1);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(parameters->sizePolicy().hasHeightForWidth());
        parameters->setSizePolicy(sizePolicy1);
        splitter->addWidget(parameters);
        preview = new omni::ui::proj::MultiSetupPreview(splitter);
        preview->setObjectName(QStringLiteral("preview"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(5);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(preview->sizePolicy().hasHeightForWidth());
        preview->setSizePolicy(sizePolicy2);
        splitter->addWidget(preview);

        horizontalLayout_2->addWidget(splitter);


        verticalLayout->addWidget(groupSetup);

        boxButtons = new QWidget(omni__ui__proj__MultiSetupDialog);
        boxButtons->setObjectName(QStringLiteral("boxButtons"));
        horizontalLayout = new QHBoxLayout(boxButtons);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(5, 5, 5, 5);
        btnCancel = new QToolButton(boxButtons);
        btnCancel->setObjectName(QStringLiteral("btnCancel"));
        btnCancel->setMinimumSize(QSize(0, 48));
        btnCancel->setAutoRaise(true);

        horizontalLayout->addWidget(btnCancel);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        btnReplace = new QToolButton(boxButtons);
        btnReplace->setObjectName(QStringLiteral("btnReplace"));
        btnReplace->setMinimumSize(QSize(0, 48));
        btnReplace->setAutoRaise(true);

        horizontalLayout->addWidget(btnReplace);

        btnAppend = new QToolButton(boxButtons);
        btnAppend->setObjectName(QStringLiteral("btnAppend"));
        btnAppend->setMinimumSize(QSize(0, 48));
        btnAppend->setAutoRaise(true);

        horizontalLayout->addWidget(btnAppend);


        verticalLayout->addWidget(boxButtons);


        retranslateUi(omni__ui__proj__MultiSetupDialog);

        QMetaObject::connectSlotsByName(omni__ui__proj__MultiSetupDialog);
    } // setupUi

    void retranslateUi(QDialog *omni__ui__proj__MultiSetupDialog)
    {
        omni__ui__proj__MultiSetupDialog->setWindowTitle(QApplication::translate("omni::ui::proj::MultiSetupDialog", "Projector Arrangement MultiSetup", 0));
        groupSetup->setTitle(QApplication::translate("omni::ui::proj::MultiSetupDialog", "MultiSetup Name", 0));
        btnCancel->setText(QApplication::translate("omni::ui::proj::MultiSetupDialog", "Cancel", 0));
        btnReplace->setText(QApplication::translate("omni::ui::proj::MultiSetupDialog", "Replace", 0));
        btnAppend->setText(QApplication::translate("omni::ui::proj::MultiSetupDialog", "Append", 0));
    } // retranslateUi

};

} // namespace proj
} // namespace ui
} // namespace omni

namespace omni {
namespace ui {
namespace proj {
namespace Ui {
    class MultiSetupDialog: public Ui_MultiSetupDialog {};
} // namespace Ui
} // namespace proj
} // namespace ui
} // namespace omni

#endif // UI_OMNI_UI_MULTISETUPDIALOG_H
