/********************************************************************************
** Form generated from reading UI file 'omni_ui_Warp.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OMNI_UI_WARP_H
#define UI_OMNI_UI_WARP_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <slim/RangedInt.h>

namespace omni {
namespace ui {

class Ui_Warp
{
public:
    QVBoxLayout *verticalLayout;
    QToolButton *btnResize;
    slim::RangedInt *sliderHorz;
    slim::RangedInt *sliderVert;
    QToolButton *btnReset;
    QComboBox *boxInterpolation;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *omni__ui__Warp)
    {
        if (omni__ui__Warp->objectName().isEmpty())
            omni__ui__Warp->setObjectName(QStringLiteral("omni__ui__Warp"));
        omni__ui__Warp->resize(615, 759);
        verticalLayout = new QVBoxLayout(omni__ui__Warp);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        btnResize = new QToolButton(omni__ui__Warp);
        btnResize->setObjectName(QStringLiteral("btnResize"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(btnResize->sizePolicy().hasHeightForWidth());
        btnResize->setSizePolicy(sizePolicy);
        btnResize->setCheckable(true);
        btnResize->setAutoRaise(true);

        verticalLayout->addWidget(btnResize);

        sliderHorz = new slim::RangedInt(omni__ui__Warp);
        sliderHorz->setObjectName(QStringLiteral("sliderHorz"));

        verticalLayout->addWidget(sliderHorz);

        sliderVert = new slim::RangedInt(omni__ui__Warp);
        sliderVert->setObjectName(QStringLiteral("sliderVert"));

        verticalLayout->addWidget(sliderVert);

        btnReset = new QToolButton(omni__ui__Warp);
        btnReset->setObjectName(QStringLiteral("btnReset"));
        sizePolicy.setHeightForWidth(btnReset->sizePolicy().hasHeightForWidth());
        btnReset->setSizePolicy(sizePolicy);
        btnReset->setAutoRaise(true);

        verticalLayout->addWidget(btnReset);

        boxInterpolation = new QComboBox(omni__ui__Warp);
        boxInterpolation->setObjectName(QStringLiteral("boxInterpolation"));

        verticalLayout->addWidget(boxInterpolation);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        retranslateUi(omni__ui__Warp);

        QMetaObject::connectSlotsByName(omni__ui__Warp);
    } // setupUi

    void retranslateUi(QWidget *omni__ui__Warp)
    {
        omni__ui__Warp->setWindowTitle(QApplication::translate("omni::ui::Warp", "Form", 0));
        btnResize->setText(QApplication::translate("omni::ui::Warp", "Resize", 0));
        btnReset->setText(QApplication::translate("omni::ui::Warp", "Reset", 0));
        boxInterpolation->clear();
        boxInterpolation->insertItems(0, QStringList()
         << QApplication::translate("omni::ui::Warp", "Bicubic Interpolation", 0)
         << QApplication::translate("omni::ui::Warp", "Linear Interpolation", 0)
        );
    } // retranslateUi

};

} // namespace ui
} // namespace omni

namespace omni {
namespace ui {
namespace Ui {
    class Warp: public Ui_Warp {};
} // namespace Ui
} // namespace ui
} // namespace omni

#endif // UI_OMNI_UI_WARP_H
