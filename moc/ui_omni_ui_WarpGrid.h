/********************************************************************************
** Form generated from reading UI file 'omni_ui_WarpGrid.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OMNI_UI_WARPGRID_H
#define UI_OMNI_UI_WARPGRID_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

namespace omni {
namespace ui {

class Ui_WarpGrid
{
public:
    QVBoxLayout *verticalLayout;
    QPushButton *btnResizeWarp;
    QFrame *warpSizeFrame;
    QFormLayout *formLayout_3;
    QLabel *lbHorizontal;
    QSpinBox *boxHorizontal;
    QLabel *lbVertical;
    QSpinBox *boxVertical;
    QPushButton *btnReset;

    void setupUi(QWidget *omni__ui__WarpGrid)
    {
        if (omni__ui__WarpGrid->objectName().isEmpty())
            omni__ui__WarpGrid->setObjectName(QStringLiteral("omni__ui__WarpGrid"));
        omni__ui__WarpGrid->resize(218, 162);
        verticalLayout = new QVBoxLayout(omni__ui__WarpGrid);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        btnResizeWarp = new QPushButton(omni__ui__WarpGrid);
        btnResizeWarp->setObjectName(QStringLiteral("btnResizeWarp"));
        btnResizeWarp->setCheckable(true);
        btnResizeWarp->setFlat(false);

        verticalLayout->addWidget(btnResizeWarp);

        warpSizeFrame = new QFrame(omni__ui__WarpGrid);
        warpSizeFrame->setObjectName(QStringLiteral("warpSizeFrame"));
        formLayout_3 = new QFormLayout(warpSizeFrame);
        formLayout_3->setObjectName(QStringLiteral("formLayout_3"));
        formLayout_3->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        lbHorizontal = new QLabel(warpSizeFrame);
        lbHorizontal->setObjectName(QStringLiteral("lbHorizontal"));

        formLayout_3->setWidget(0, QFormLayout::LabelRole, lbHorizontal);

        boxHorizontal = new QSpinBox(warpSizeFrame);
        boxHorizontal->setObjectName(QStringLiteral("boxHorizontal"));
        boxHorizontal->setMinimum(2);
        boxHorizontal->setMaximum(12);
        boxHorizontal->setValue(6);

        formLayout_3->setWidget(0, QFormLayout::FieldRole, boxHorizontal);

        lbVertical = new QLabel(warpSizeFrame);
        lbVertical->setObjectName(QStringLiteral("lbVertical"));

        formLayout_3->setWidget(1, QFormLayout::LabelRole, lbVertical);

        boxVertical = new QSpinBox(warpSizeFrame);
        boxVertical->setObjectName(QStringLiteral("boxVertical"));
        boxVertical->setMinimum(2);
        boxVertical->setMaximum(12);
        boxVertical->setValue(6);

        formLayout_3->setWidget(1, QFormLayout::FieldRole, boxVertical);


        verticalLayout->addWidget(warpSizeFrame);

        btnReset = new QPushButton(omni__ui__WarpGrid);
        btnReset->setObjectName(QStringLiteral("btnReset"));

        verticalLayout->addWidget(btnReset);


        retranslateUi(omni__ui__WarpGrid);

        QMetaObject::connectSlotsByName(omni__ui__WarpGrid);
    } // setupUi

    void retranslateUi(QWidget *omni__ui__WarpGrid)
    {
        omni__ui__WarpGrid->setWindowTitle(QApplication::translate("omni::ui::WarpGrid", "Form", 0));
        btnResizeWarp->setText(QApplication::translate("omni::ui::WarpGrid", "Resize", 0));
        lbHorizontal->setText(QApplication::translate("omni::ui::WarpGrid", "Horizontal", 0));
        lbVertical->setText(QApplication::translate("omni::ui::WarpGrid", "Vertical", 0));
        btnReset->setText(QApplication::translate("omni::ui::WarpGrid", "Reset", 0));
    } // retranslateUi

};

} // namespace ui
} // namespace omni

namespace omni {
namespace ui {
namespace Ui {
    class WarpGrid: public Ui_WarpGrid {};
} // namespace Ui
} // namespace ui
} // namespace omni

#endif // UI_OMNI_UI_WARPGRID_H
