/********************************************************************************
** Form generated from reading UI file 'omni_ui_ScreenSettings.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OMNI_UI_SCREENSETTINGS_H
#define UI_OMNI_UI_SCREENSETTINGS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Form
{
public:
    QPushButton *btnIdentify;
    QPushButton *btnUpdateScreens;

    void setupUi(QWidget *Form)
    {
        if (Form->objectName().isEmpty())
            Form->setObjectName(QStringLiteral("Form"));
        Form->resize(319, 114);
        btnIdentify = new QPushButton(Form);
        btnIdentify->setObjectName(QStringLiteral("btnIdentify"));
        btnIdentify->setGeometry(QRect(10, 58, 274, 32));
        btnIdentify->setCheckable(true);
        btnUpdateScreens = new QPushButton(Form);
        btnUpdateScreens->setObjectName(QStringLiteral("btnUpdateScreens"));
        btnUpdateScreens->setGeometry(QRect(10, 20, 274, 32));

        retranslateUi(Form);

        QMetaObject::connectSlotsByName(Form);
    } // setupUi

    void retranslateUi(QWidget *Form)
    {
        Form->setWindowTitle(QApplication::translate("Form", "Form", 0));
        btnIdentify->setText(QApplication::translate("Form", "Identify", 0));
        btnUpdateScreens->setText(QApplication::translate("Form", "Update Screens", 0));
    } // retranslateUi

};

namespace Ui {
    class Form: public Ui_Form {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OMNI_UI_SCREENSETTINGS_H
