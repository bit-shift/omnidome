/********************************************************************************
** Form generated from reading UI file 'omni_ui_BlendMaskSettings.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OMNI_UI_BLENDMASKSETTINGS_H
#define UI_OMNI_UI_BLENDMASKSETTINGS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>
#include <omni/ui/BrushPreview.h>

QT_BEGIN_NAMESPACE

class Ui_Form
{
public:
    QGroupBox *grpBrush;
    QFormLayout *formLayout_2;
    QFrame *horizontalFrame;
    QHBoxLayout *horizontalLayout_7;
    QSpacerItem *horizontalSpacer;
    omni::ui::BrushPreview *brushPreview;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_2;
    QLabel *lbSize;
    QSlider *sliderBrushSize;
    QLabel *lbFeather;
    QSlider *sliderFeather;
    QCheckBox *chkBrushInvert;
    QGroupBox *grpEdgeMask;
    QFormLayout *formLayout_5;
    QLabel *lbGamma;
    QSlider *sliderGamma;
    QComboBox *boxMaskColor;

    void setupUi(QWidget *Form)
    {
        if (Form->objectName().isEmpty())
            Form->setObjectName(QStringLiteral("Form"));
        Form->resize(717, 724);
        grpBrush = new QGroupBox(Form);
        grpBrush->setObjectName(QStringLiteral("grpBrush"));
        grpBrush->setGeometry(QRect(30, 10, 230, 216));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(grpBrush->sizePolicy().hasHeightForWidth());
        grpBrush->setSizePolicy(sizePolicy);
        formLayout_2 = new QFormLayout(grpBrush);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        formLayout_2->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        horizontalFrame = new QFrame(grpBrush);
        horizontalFrame->setObjectName(QStringLiteral("horizontalFrame"));
        horizontalLayout_7 = new QHBoxLayout(horizontalFrame);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer);

        brushPreview = new omni::ui::BrushPreview(horizontalFrame);
        brushPreview->setObjectName(QStringLiteral("brushPreview"));
        brushPreview->setMinimumSize(QSize(64, 64));
        brushPreview->setMaximumSize(QSize(64, 64));

        horizontalLayout_7->addWidget(brushPreview);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_2);


        formLayout_2->setWidget(0, QFormLayout::FieldRole, horizontalFrame);

        label_2 = new QLabel(grpBrush);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_2);

        lbSize = new QLabel(grpBrush);
        lbSize->setObjectName(QStringLiteral("lbSize"));
        lbSize->setPixmap(QPixmap(QString::fromUtf8(":/buttons/rc/buttons/brush_size.png")));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, lbSize);

        sliderBrushSize = new QSlider(grpBrush);
        sliderBrushSize->setObjectName(QStringLiteral("sliderBrushSize"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(sliderBrushSize->sizePolicy().hasHeightForWidth());
        sliderBrushSize->setSizePolicy(sizePolicy1);
        sliderBrushSize->setMinimumSize(QSize(32, 0));
        sliderBrushSize->setMaximumSize(QSize(16777215, 16777215));
        sliderBrushSize->setMinimum(10);
        sliderBrushSize->setMaximum(100);
        sliderBrushSize->setValue(50);
        sliderBrushSize->setOrientation(Qt::Horizontal);

        formLayout_2->setWidget(2, QFormLayout::FieldRole, sliderBrushSize);

        lbFeather = new QLabel(grpBrush);
        lbFeather->setObjectName(QStringLiteral("lbFeather"));
        lbFeather->setPixmap(QPixmap(QString::fromUtf8(":/buttons/rc/buttons/FEATHER.png")));
        lbFeather->setScaledContents(true);

        formLayout_2->setWidget(3, QFormLayout::LabelRole, lbFeather);

        sliderFeather = new QSlider(grpBrush);
        sliderFeather->setObjectName(QStringLiteral("sliderFeather"));
        sizePolicy1.setHeightForWidth(sliderFeather->sizePolicy().hasHeightForWidth());
        sliderFeather->setSizePolicy(sizePolicy1);
        sliderFeather->setMaximumSize(QSize(16777215, 16777215));
        sliderFeather->setMaximum(200);
        sliderFeather->setValue(100);
        sliderFeather->setOrientation(Qt::Horizontal);

        formLayout_2->setWidget(3, QFormLayout::FieldRole, sliderFeather);

        chkBrushInvert = new QCheckBox(grpBrush);
        chkBrushInvert->setObjectName(QStringLiteral("chkBrushInvert"));

        formLayout_2->setWidget(1, QFormLayout::FieldRole, chkBrushInvert);

        grpEdgeMask = new QGroupBox(Form);
        grpEdgeMask->setObjectName(QStringLiteral("grpEdgeMask"));
        grpEdgeMask->setGeometry(QRect(20, 250, 230, 193));
        sizePolicy.setHeightForWidth(grpEdgeMask->sizePolicy().hasHeightForWidth());
        grpEdgeMask->setSizePolicy(sizePolicy);
        formLayout_5 = new QFormLayout(grpEdgeMask);
        formLayout_5->setObjectName(QStringLiteral("formLayout_5"));
        formLayout_5->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        formLayout_5->setVerticalSpacing(3);
        lbGamma = new QLabel(grpEdgeMask);
        lbGamma->setObjectName(QStringLiteral("lbGamma"));
        lbGamma->setPixmap(QPixmap(QString::fromUtf8(":/qss_icons/rc/buttons/GAMMA.svg")));

        formLayout_5->setWidget(0, QFormLayout::LabelRole, lbGamma);

        sliderGamma = new QSlider(grpEdgeMask);
        sliderGamma->setObjectName(QStringLiteral("sliderGamma"));
        sliderGamma->setMaximumSize(QSize(16777215, 16777215));
        sliderGamma->setMinimum(-100);
        sliderGamma->setMaximum(100);
        sliderGamma->setOrientation(Qt::Horizontal);

        formLayout_5->setWidget(0, QFormLayout::FieldRole, sliderGamma);

        boxMaskColor = new QComboBox(grpEdgeMask);
        boxMaskColor->setObjectName(QStringLiteral("boxMaskColor"));

        formLayout_5->setWidget(1, QFormLayout::FieldRole, boxMaskColor);


        retranslateUi(Form);

        QMetaObject::connectSlotsByName(Form);
    } // setupUi

    void retranslateUi(QWidget *Form)
    {
        Form->setWindowTitle(QApplication::translate("Form", "Form", 0));
        grpBrush->setTitle(QApplication::translate("Form", "Brush", 0));
        label_2->setText(QApplication::translate("Form", "Invert", 0));
#ifndef QT_NO_TOOLTIP
        lbSize->setToolTip(QApplication::translate("Form", "Brush size", 0));
#endif // QT_NO_TOOLTIP
        lbSize->setText(QString());
#ifndef QT_NO_TOOLTIP
        lbFeather->setToolTip(QApplication::translate("Form", "Brush feather", 0));
#endif // QT_NO_TOOLTIP
        lbFeather->setText(QString());
        chkBrushInvert->setText(QString());
        grpEdgeMask->setTitle(QApplication::translate("Form", "Edge Mask", 0));
#ifndef QT_NO_TOOLTIP
        lbGamma->setToolTip(QApplication::translate("Form", "Gamma", 0));
#endif // QT_NO_TOOLTIP
        lbGamma->setText(QString());
        boxMaskColor->clear();
        boxMaskColor->insertItems(0, QStringList()
         << QApplication::translate("Form", "Colored Mask", 0)
         << QApplication::translate("Form", "White Mask", 0)
        );
    } // retranslateUi

};

namespace Ui {
    class Form: public Ui_Form {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OMNI_UI_BLENDMASKSETTINGS_H
