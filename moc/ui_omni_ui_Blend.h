/********************************************************************************
** Form generated from reading UI file 'omni_ui_Blend.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OMNI_UI_BLEND_H
#define UI_OMNI_UI_BLEND_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <slim/RangedFloat.h>
#include "BrushPreview.h"

namespace omni {
namespace ui {

class Ui_Blend
{
public:
    QVBoxLayout *verticalLayout;
    QGroupBox *grpBrush;
    QVBoxLayout *verticalLayout_3;
    omni::ui::BrushPreview *brushPreview;
    QCheckBox *chkInvert;
    slim::RangedFloat *sliderSize;
    slim::RangedFloat *sliderFeather;
    QGroupBox *grpEdgeMask;
    QVBoxLayout *verticalLayout_2;
    slim::RangedFloat *sliderGamma;
    slim::RangedFloat *sliderTop;
    slim::RangedFloat *sliderLeft;
    slim::RangedFloat *sliderRight;
    slim::RangedFloat *sliderBottom;
    QComboBox *boxMaskColor;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *omni__ui__Blend)
    {
        if (omni__ui__Blend->objectName().isEmpty())
            omni__ui__Blend->setObjectName(QStringLiteral("omni__ui__Blend"));
        omni__ui__Blend->resize(469, 1457);
        verticalLayout = new QVBoxLayout(omni__ui__Blend);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        grpBrush = new QGroupBox(omni__ui__Blend);
        grpBrush->setObjectName(QStringLiteral("grpBrush"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(grpBrush->sizePolicy().hasHeightForWidth());
        grpBrush->setSizePolicy(sizePolicy);
        verticalLayout_3 = new QVBoxLayout(grpBrush);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        brushPreview = new omni::ui::BrushPreview(grpBrush);
        brushPreview->setObjectName(QStringLiteral("brushPreview"));
        sizePolicy.setHeightForWidth(brushPreview->sizePolicy().hasHeightForWidth());
        brushPreview->setSizePolicy(sizePolicy);

        verticalLayout_3->addWidget(brushPreview);

        chkInvert = new QCheckBox(grpBrush);
        chkInvert->setObjectName(QStringLiteral("chkInvert"));

        verticalLayout_3->addWidget(chkInvert);

        sliderSize = new slim::RangedFloat(grpBrush);
        sliderSize->setObjectName(QStringLiteral("sliderSize"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(sliderSize->sizePolicy().hasHeightForWidth());
        sliderSize->setSizePolicy(sizePolicy1);

        verticalLayout_3->addWidget(sliderSize);

        sliderFeather = new slim::RangedFloat(grpBrush);
        sliderFeather->setObjectName(QStringLiteral("sliderFeather"));
        sizePolicy1.setHeightForWidth(sliderFeather->sizePolicy().hasHeightForWidth());
        sliderFeather->setSizePolicy(sizePolicy1);

        verticalLayout_3->addWidget(sliderFeather);


        verticalLayout->addWidget(grpBrush);

        grpEdgeMask = new QGroupBox(omni__ui__Blend);
        grpEdgeMask->setObjectName(QStringLiteral("grpEdgeMask"));
        sizePolicy1.setHeightForWidth(grpEdgeMask->sizePolicy().hasHeightForWidth());
        grpEdgeMask->setSizePolicy(sizePolicy1);
        verticalLayout_2 = new QVBoxLayout(grpEdgeMask);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        sliderGamma = new slim::RangedFloat(grpEdgeMask);
        sliderGamma->setObjectName(QStringLiteral("sliderGamma"));

        verticalLayout_2->addWidget(sliderGamma);

        sliderTop = new slim::RangedFloat(grpEdgeMask);
        sliderTop->setObjectName(QStringLiteral("sliderTop"));

        verticalLayout_2->addWidget(sliderTop);

        sliderLeft = new slim::RangedFloat(grpEdgeMask);
        sliderLeft->setObjectName(QStringLiteral("sliderLeft"));

        verticalLayout_2->addWidget(sliderLeft);

        sliderRight = new slim::RangedFloat(grpEdgeMask);
        sliderRight->setObjectName(QStringLiteral("sliderRight"));

        verticalLayout_2->addWidget(sliderRight);

        sliderBottom = new slim::RangedFloat(grpEdgeMask);
        sliderBottom->setObjectName(QStringLiteral("sliderBottom"));

        verticalLayout_2->addWidget(sliderBottom);

        boxMaskColor = new QComboBox(grpEdgeMask);
        boxMaskColor->setObjectName(QStringLiteral("boxMaskColor"));

        verticalLayout_2->addWidget(boxMaskColor);


        verticalLayout->addWidget(grpEdgeMask);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        retranslateUi(omni__ui__Blend);

        QMetaObject::connectSlotsByName(omni__ui__Blend);
    } // setupUi

    void retranslateUi(QWidget *omni__ui__Blend)
    {
        omni__ui__Blend->setWindowTitle(QApplication::translate("omni::ui::Blend", "Form", 0));
        grpBrush->setTitle(QApplication::translate("omni::ui::Blend", "Brush", 0));
        chkInvert->setText(QApplication::translate("omni::ui::Blend", "Invert", 0));
        grpEdgeMask->setTitle(QApplication::translate("omni::ui::Blend", "Edge Mask", 0));
        boxMaskColor->clear();
        boxMaskColor->insertItems(0, QStringList()
         << QApplication::translate("omni::ui::Blend", "Colored Mask", 0)
         << QApplication::translate("omni::ui::Blend", "White Mask", 0)
         << QApplication::translate("omni::ui::Blend", "Mask with Input", 0)
        );
        boxMaskColor->setCurrentText(QApplication::translate("omni::ui::Blend", "Colored Mask", 0));
    } // retranslateUi

};

} // namespace ui
} // namespace omni

namespace omni {
namespace ui {
namespace Ui {
    class Blend: public Ui_Blend {};
} // namespace Ui
} // namespace ui
} // namespace omni

#endif // UI_OMNI_UI_BLEND_H
