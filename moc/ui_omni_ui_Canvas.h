/********************************************************************************
** Form generated from reading UI file 'omni_ui_Canvas.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OMNI_UI_CANVAS_H
#define UI_OMNI_UI_CANVAS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

namespace omni {
namespace ui {

class Ui_Canvas
{
public:
    QVBoxLayout *verticalLayout;
    QComboBox *boxCanvasSelect;
    QCheckBox *chkDisplayInput;
    QComboBox *boxProjectorViewMode;

    void setupUi(QWidget *omni__ui__Canvas)
    {
        if (omni__ui__Canvas->objectName().isEmpty())
            omni__ui__Canvas->setObjectName(QStringLiteral("omni__ui__Canvas"));
        omni__ui__Canvas->resize(399, 382);
        verticalLayout = new QVBoxLayout(omni__ui__Canvas);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        boxCanvasSelect = new QComboBox(omni__ui__Canvas);
        boxCanvasSelect->setObjectName(QStringLiteral("boxCanvasSelect"));

        verticalLayout->addWidget(boxCanvasSelect);

        chkDisplayInput = new QCheckBox(omni__ui__Canvas);
        chkDisplayInput->setObjectName(QStringLiteral("chkDisplayInput"));
        chkDisplayInput->setChecked(true);

        verticalLayout->addWidget(chkDisplayInput);

        boxProjectorViewMode = new QComboBox(omni__ui__Canvas);
        boxProjectorViewMode->setObjectName(QStringLiteral("boxProjectorViewMode"));

        verticalLayout->addWidget(boxProjectorViewMode);


        retranslateUi(omni__ui__Canvas);

        QMetaObject::connectSlotsByName(omni__ui__Canvas);
    } // setupUi

    void retranslateUi(QWidget *omni__ui__Canvas)
    {
        omni__ui__Canvas->setWindowTitle(QApplication::translate("omni::ui::Canvas", "Form", 0));
        chkDisplayInput->setText(QApplication::translate("omni::ui::Canvas", "Display Input", 0));
        boxProjectorViewMode->clear();
        boxProjectorViewMode->insertItems(0, QStringList()
         << QApplication::translate("omni::ui::Canvas", "Inside", 0)
         << QApplication::translate("omni::ui::Canvas", "Outside", 0)
         << QApplication::translate("omni::ui::Canvas", "Both sides", 0)
        );
    } // retranslateUi

};

} // namespace ui
} // namespace omni

namespace omni {
namespace ui {
namespace Ui {
    class Canvas: public Ui_Canvas {};
} // namespace Ui
} // namespace ui
} // namespace omni

#endif // UI_OMNI_UI_CANVAS_H
