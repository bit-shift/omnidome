/********************************************************************************
** Form generated from reading UI file 'omni_ui_MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OMNI_UI_MAINWINDOW_H
#define UI_OMNI_UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <proj/TuningList.h>
#include "Blend.h"
#include "Canvas.h"
#include "Input.h"
#include "Mapping.h"
#include "Warp.h"

namespace omni {
namespace ui {

class Ui_MainWindow
{
public:
    QAction *actionNew;
    QAction *actionWarp;
    QAction *actionOpen;
    QAction *actionSave;
    QAction *actionScreenSetup;
    QAction *actionProjectionSetup;
    QAction *actionBlend;
    QAction *actionExport;
    QAction *actionEditAsNew;
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout_3;
    QFrame *frameToolBar;
    QHBoxLayout *horizontalLayout;
    QToolButton *btnAbout;
    QToolButton *btnNew;
    QToolButton *btnOpen;
    QToolButton *btnSave;
    QToolButton *btnEditAsNew;
    QSpacerItem *horizontalSpacer;
    QToolButton *btnScreenSetup;
    QToolButton *btnProjectionSetup;
    QToolButton *btnWarp;
    QToolButton *btnBlend;
    QToolButton *btnExport;
    QFrame *horizontalFrame;
    QHBoxLayout *horizontalLayout_2;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QVBoxLayout *verticalLayout_2;
    omni::ui::Canvas *grpCanvas;
    omni::ui::Blend *grpBlend;
    omni::ui::Warp *grpWarp;
    omni::ui::Input *grpInputs;
    QSpacerItem *verticalSpacer;
    omni::ui::Mapping *grpMapping;
    QWidget *pages;
    QGroupBox *grpProjectors;
    QVBoxLayout *verticalLayout;
    omni::ui::proj::TuningList *tuningList;
    QToolButton *btnAddTuning;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *omni__ui__MainWindow)
    {
        if (omni__ui__MainWindow->objectName().isEmpty())
            omni__ui__MainWindow->setObjectName(QStringLiteral("omni__ui__MainWindow"));
        omni__ui__MainWindow->resize(1163, 580);
        omni__ui__MainWindow->setFocusPolicy(Qt::TabFocus);
        omni__ui__MainWindow->setContextMenuPolicy(Qt::NoContextMenu);
        actionNew = new QAction(omni__ui__MainWindow);
        actionNew->setObjectName(QStringLiteral("actionNew"));
        actionNew->setShortcutContext(Qt::ApplicationShortcut);
        actionWarp = new QAction(omni__ui__MainWindow);
        actionWarp->setObjectName(QStringLiteral("actionWarp"));
        actionWarp->setShortcutContext(Qt::ApplicationShortcut);
        actionOpen = new QAction(omni__ui__MainWindow);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionOpen->setShortcutContext(Qt::ApplicationShortcut);
        actionSave = new QAction(omni__ui__MainWindow);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionSave->setShortcutContext(Qt::ApplicationShortcut);
        actionScreenSetup = new QAction(omni__ui__MainWindow);
        actionScreenSetup->setObjectName(QStringLiteral("actionScreenSetup"));
        actionScreenSetup->setShortcutContext(Qt::ApplicationShortcut);
        actionProjectionSetup = new QAction(omni__ui__MainWindow);
        actionProjectionSetup->setObjectName(QStringLiteral("actionProjectionSetup"));
        actionProjectionSetup->setShortcutContext(Qt::ApplicationShortcut);
        actionBlend = new QAction(omni__ui__MainWindow);
        actionBlend->setObjectName(QStringLiteral("actionBlend"));
        actionBlend->setShortcutContext(Qt::ApplicationShortcut);
        actionExport = new QAction(omni__ui__MainWindow);
        actionExport->setObjectName(QStringLiteral("actionExport"));
        actionExport->setShortcutContext(Qt::ApplicationShortcut);
        actionEditAsNew = new QAction(omni__ui__MainWindow);
        actionEditAsNew->setObjectName(QStringLiteral("actionEditAsNew"));
        actionEditAsNew->setShortcutContext(Qt::ApplicationShortcut);
        actionEditAsNew->setPriority(QAction::HighPriority);
        centralwidget = new QWidget(omni__ui__MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        verticalLayout_3 = new QVBoxLayout(centralwidget);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(5, 5, 5, 5);
        frameToolBar = new QFrame(centralwidget);
        frameToolBar->setObjectName(QStringLiteral("frameToolBar"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(frameToolBar->sizePolicy().hasHeightForWidth());
        frameToolBar->setSizePolicy(sizePolicy);
        horizontalLayout = new QHBoxLayout(frameToolBar);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        btnAbout = new QToolButton(frameToolBar);
        btnAbout->setObjectName(QStringLiteral("btnAbout"));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(btnAbout->sizePolicy().hasHeightForWidth());
        btnAbout->setSizePolicy(sizePolicy1);
        QFont font;
        font.setPointSize(1);
        btnAbout->setFont(font);
        QIcon icon;
        icon.addFile(QStringLiteral(":/logo/omni1rainbow.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnAbout->setIcon(icon);
        btnAbout->setIconSize(QSize(56, 56));
        btnAbout->setPopupMode(QToolButton::InstantPopup);
        btnAbout->setToolButtonStyle(Qt::ToolButtonIconOnly);
        btnAbout->setAutoRaise(true);
        btnAbout->setArrowType(Qt::NoArrow);

        horizontalLayout->addWidget(btnAbout);

        btnNew = new QToolButton(frameToolBar);
        btnNew->setObjectName(QStringLiteral("btnNew"));
        btnNew->setMinimumSize(QSize(64, 64));
        QFont font1;
        font1.setFamily(QStringLiteral("Arial Black"));
        font1.setPointSize(12);
        btnNew->setFont(font1);
        btnNew->setIconSize(QSize(54, 54));
        btnNew->setAutoRaise(true);

        horizontalLayout->addWidget(btnNew);

        btnOpen = new QToolButton(frameToolBar);
        btnOpen->setObjectName(QStringLiteral("btnOpen"));
        btnOpen->setMinimumSize(QSize(64, 64));
        btnOpen->setFont(font1);
        btnOpen->setIconSize(QSize(54, 54));
        btnOpen->setAutoRaise(true);

        horizontalLayout->addWidget(btnOpen);

        btnSave = new QToolButton(frameToolBar);
        btnSave->setObjectName(QStringLiteral("btnSave"));
        btnSave->setMinimumSize(QSize(64, 64));
        btnSave->setFont(font1);
        btnSave->setIconSize(QSize(54, 54));
        btnSave->setAutoRaise(true);

        horizontalLayout->addWidget(btnSave);

        btnEditAsNew = new QToolButton(frameToolBar);
        btnEditAsNew->setObjectName(QStringLiteral("btnEditAsNew"));
        btnEditAsNew->setMinimumSize(QSize(64, 64));
        btnEditAsNew->setFont(font1);
        btnEditAsNew->setIconSize(QSize(54, 54));
        btnEditAsNew->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        btnEditAsNew->setAutoRaise(true);

        horizontalLayout->addWidget(btnEditAsNew);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        btnScreenSetup = new QToolButton(frameToolBar);
        btnScreenSetup->setObjectName(QStringLiteral("btnScreenSetup"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(btnScreenSetup->sizePolicy().hasHeightForWidth());
        btnScreenSetup->setSizePolicy(sizePolicy2);
        btnScreenSetup->setMinimumSize(QSize(64, 64));
        btnScreenSetup->setFont(font1);
        btnScreenSetup->setCheckable(false);
        btnScreenSetup->setAutoRaise(true);
        btnScreenSetup->setArrowType(Qt::NoArrow);

        horizontalLayout->addWidget(btnScreenSetup);

        btnProjectionSetup = new QToolButton(frameToolBar);
        btnProjectionSetup->setObjectName(QStringLiteral("btnProjectionSetup"));
        sizePolicy2.setHeightForWidth(btnProjectionSetup->sizePolicy().hasHeightForWidth());
        btnProjectionSetup->setSizePolicy(sizePolicy2);
        btnProjectionSetup->setMinimumSize(QSize(64, 64));
        btnProjectionSetup->setFont(font1);
        btnProjectionSetup->setAutoRaise(true);

        horizontalLayout->addWidget(btnProjectionSetup);

        btnWarp = new QToolButton(frameToolBar);
        btnWarp->setObjectName(QStringLiteral("btnWarp"));
        sizePolicy2.setHeightForWidth(btnWarp->sizePolicy().hasHeightForWidth());
        btnWarp->setSizePolicy(sizePolicy2);
        btnWarp->setMinimumSize(QSize(64, 64));
        btnWarp->setFont(font1);
        btnWarp->setAutoRaise(true);

        horizontalLayout->addWidget(btnWarp);

        btnBlend = new QToolButton(frameToolBar);
        btnBlend->setObjectName(QStringLiteral("btnBlend"));
        sizePolicy2.setHeightForWidth(btnBlend->sizePolicy().hasHeightForWidth());
        btnBlend->setSizePolicy(sizePolicy2);
        btnBlend->setMinimumSize(QSize(64, 64));
        btnBlend->setFont(font1);
        btnBlend->setAutoRaise(true);

        horizontalLayout->addWidget(btnBlend);

        btnExport = new QToolButton(frameToolBar);
        btnExport->setObjectName(QStringLiteral("btnExport"));
        sizePolicy2.setHeightForWidth(btnExport->sizePolicy().hasHeightForWidth());
        btnExport->setSizePolicy(sizePolicy2);
        btnExport->setMinimumSize(QSize(64, 64));
        btnExport->setFont(font1);
        btnExport->setAutoRaise(true);

        horizontalLayout->addWidget(btnExport);


        verticalLayout_3->addWidget(frameToolBar);

        horizontalFrame = new QFrame(centralwidget);
        horizontalFrame->setObjectName(QStringLiteral("horizontalFrame"));
        horizontalFrame->setFrameShape(QFrame::StyledPanel);
        horizontalFrame->setFrameShadow(QFrame::Sunken);
        horizontalFrame->setMidLineWidth(1);
        horizontalLayout_2 = new QHBoxLayout(horizontalFrame);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(7, 7, 7, 7);
        scrollArea = new QScrollArea(horizontalFrame);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy3.setHorizontalStretch(4);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(scrollArea->sizePolicy().hasHeightForWidth());
        scrollArea->setSizePolicy(sizePolicy3);
        scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        scrollArea->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContentsOnFirstShow);
        scrollArea->setWidgetResizable(true);
        scrollArea->setAlignment(Qt::AlignJustify|Qt::AlignVCenter);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 233, 417));
        QSizePolicy sizePolicy4(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy4.setHorizontalStretch(1);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(scrollAreaWidgetContents->sizePolicy().hasHeightForWidth());
        scrollAreaWidgetContents->setSizePolicy(sizePolicy4);
        verticalLayout_2 = new QVBoxLayout(scrollAreaWidgetContents);
        verticalLayout_2->setSpacing(4);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(3, 3, 3, 3);
        grpCanvas = new omni::ui::Canvas(scrollAreaWidgetContents);
        grpCanvas->setObjectName(QStringLiteral("grpCanvas"));
        QSizePolicy sizePolicy5(QSizePolicy::Preferred, QSizePolicy::Maximum);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(1);
        sizePolicy5.setHeightForWidth(grpCanvas->sizePolicy().hasHeightForWidth());
        grpCanvas->setSizePolicy(sizePolicy5);

        verticalLayout_2->addWidget(grpCanvas);

        grpBlend = new omni::ui::Blend(scrollAreaWidgetContents);
        grpBlend->setObjectName(QStringLiteral("grpBlend"));
        QSizePolicy sizePolicy6(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy6.setHorizontalStretch(0);
        sizePolicy6.setVerticalStretch(1);
        sizePolicy6.setHeightForWidth(grpBlend->sizePolicy().hasHeightForWidth());
        grpBlend->setSizePolicy(sizePolicy6);

        verticalLayout_2->addWidget(grpBlend);

        grpWarp = new omni::ui::Warp(scrollAreaWidgetContents);
        grpWarp->setObjectName(QStringLiteral("grpWarp"));
        sizePolicy6.setHeightForWidth(grpWarp->sizePolicy().hasHeightForWidth());
        grpWarp->setSizePolicy(sizePolicy6);

        verticalLayout_2->addWidget(grpWarp);

        grpInputs = new omni::ui::Input(scrollAreaWidgetContents);
        grpInputs->setObjectName(QStringLiteral("grpInputs"));
        QSizePolicy sizePolicy7(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy7.setHorizontalStretch(0);
        sizePolicy7.setVerticalStretch(1);
        sizePolicy7.setHeightForWidth(grpInputs->sizePolicy().hasHeightForWidth());
        grpInputs->setSizePolicy(sizePolicy7);

        verticalLayout_2->addWidget(grpInputs);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        grpMapping = new omni::ui::Mapping(scrollAreaWidgetContents);
        grpMapping->setObjectName(QStringLiteral("grpMapping"));
        QSizePolicy sizePolicy8(QSizePolicy::Minimum, QSizePolicy::Maximum);
        sizePolicy8.setHorizontalStretch(0);
        sizePolicy8.setVerticalStretch(1);
        sizePolicy8.setHeightForWidth(grpMapping->sizePolicy().hasHeightForWidth());
        grpMapping->setSizePolicy(sizePolicy8);

        verticalLayout_2->addWidget(grpMapping);

        scrollArea->setWidget(scrollAreaWidgetContents);

        horizontalLayout_2->addWidget(scrollArea);

        pages = new QWidget(horizontalFrame);
        pages->setObjectName(QStringLiteral("pages"));
        QSizePolicy sizePolicy9(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy9.setHorizontalStretch(12);
        sizePolicy9.setVerticalStretch(0);
        sizePolicy9.setHeightForWidth(pages->sizePolicy().hasHeightForWidth());
        pages->setSizePolicy(sizePolicy9);

        horizontalLayout_2->addWidget(pages);

        grpProjectors = new QGroupBox(horizontalFrame);
        grpProjectors->setObjectName(QStringLiteral("grpProjectors"));
        QSizePolicy sizePolicy10(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy10.setHorizontalStretch(3);
        sizePolicy10.setVerticalStretch(0);
        sizePolicy10.setHeightForWidth(grpProjectors->sizePolicy().hasHeightForWidth());
        grpProjectors->setSizePolicy(sizePolicy10);
        verticalLayout = new QVBoxLayout(grpProjectors);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 9, 0, 0);
        tuningList = new omni::ui::proj::TuningList(grpProjectors);
        tuningList->setObjectName(QStringLiteral("tuningList"));
        QSizePolicy sizePolicy11(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy11.setHorizontalStretch(0);
        sizePolicy11.setVerticalStretch(0);
        sizePolicy11.setHeightForWidth(tuningList->sizePolicy().hasHeightForWidth());
        tuningList->setSizePolicy(sizePolicy11);

        verticalLayout->addWidget(tuningList);

        btnAddTuning = new QToolButton(grpProjectors);
        btnAddTuning->setObjectName(QStringLiteral("btnAddTuning"));
        sizePolicy2.setHeightForWidth(btnAddTuning->sizePolicy().hasHeightForWidth());
        btnAddTuning->setSizePolicy(sizePolicy2);
        btnAddTuning->setPopupMode(QToolButton::MenuButtonPopup);
        btnAddTuning->setAutoRaise(true);

        verticalLayout->addWidget(btnAddTuning);


        horizontalLayout_2->addWidget(grpProjectors);


        verticalLayout_3->addWidget(horizontalFrame);

        omni__ui__MainWindow->setCentralWidget(centralwidget);
        statusbar = new QStatusBar(omni__ui__MainWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        omni__ui__MainWindow->setStatusBar(statusbar);

        retranslateUi(omni__ui__MainWindow);
        QObject::connect(btnAbout, SIGNAL(clicked()), omni__ui__MainWindow, SLOT(showAbout()));
        QObject::connect(btnNew, SIGNAL(clicked()), omni__ui__MainWindow, SLOT(newProjection()));
        QObject::connect(btnOpen, SIGNAL(clicked()), omni__ui__MainWindow, SLOT(openProjection()));
        QObject::connect(btnProjectionSetup, SIGNAL(clicked()), omni__ui__MainWindow, SLOT(showProjectionSetup()));
        QObject::connect(btnScreenSetup, SIGNAL(clicked()), omni__ui__MainWindow, SLOT(showScreenSetup()));
        QObject::connect(btnEditAsNew, SIGNAL(clicked()), omni__ui__MainWindow, SLOT(editAsNew()));
        QObject::connect(btnSave, SIGNAL(clicked()), omni__ui__MainWindow, SLOT(saveProjection()));
        QObject::connect(btnWarp, SIGNAL(clicked()), omni__ui__MainWindow, SLOT(showWarp()));
        QObject::connect(btnBlend, SIGNAL(clicked()), omni__ui__MainWindow, SLOT(showBlend()));
        QObject::connect(btnExport, SIGNAL(clicked()), omni__ui__MainWindow, SLOT(showExport()));
        QObject::connect(actionBlend, SIGNAL(triggered()), omni__ui__MainWindow, SLOT(showBlend()));
        QObject::connect(actionEditAsNew, SIGNAL(triggered()), omni__ui__MainWindow, SLOT(editAsNew()));
        QObject::connect(actionExport, SIGNAL(triggered()), omni__ui__MainWindow, SLOT(showExport()));
        QObject::connect(actionNew, SIGNAL(triggered()), omni__ui__MainWindow, SLOT(newProjection()));
        QObject::connect(actionOpen, SIGNAL(triggered()), omni__ui__MainWindow, SLOT(openProjection()));
        QObject::connect(actionSave, SIGNAL(triggered()), omni__ui__MainWindow, SLOT(saveProjection()));
        QObject::connect(actionProjectionSetup, SIGNAL(triggered()), omni__ui__MainWindow, SLOT(showProjectionSetup()));
        QObject::connect(actionWarp, SIGNAL(triggered()), omni__ui__MainWindow, SLOT(showWarp()));
        QObject::connect(actionScreenSetup, SIGNAL(triggered()), omni__ui__MainWindow, SLOT(showScreenSetup()));

        QMetaObject::connectSlotsByName(omni__ui__MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *omni__ui__MainWindow)
    {
        omni__ui__MainWindow->setWindowTitle(QApplication::translate("omni::ui::MainWindow", "Omnidome", 0));
        actionNew->setText(QApplication::translate("omni::ui::MainWindow", "New", 0));
#ifndef QT_NO_TOOLTIP
        actionNew->setToolTip(QApplication::translate("omni::ui::MainWindow", "New calibration", 0));
#endif // QT_NO_TOOLTIP
        actionNew->setShortcut(QApplication::translate("omni::ui::MainWindow", "Ctrl+N", 0));
        actionWarp->setText(QApplication::translate("omni::ui::MainWindow", "Warp", 0));
#ifndef QT_NO_TOOLTIP
        actionWarp->setToolTip(QApplication::translate("omni::ui::MainWindow", "Edit warp grids of projectors", 0));
#endif // QT_NO_TOOLTIP
        actionWarp->setShortcut(QApplication::translate("omni::ui::MainWindow", "Ctrl+3", 0));
        actionOpen->setText(QApplication::translate("omni::ui::MainWindow", "Open...", 0));
#ifndef QT_NO_TOOLTIP
        actionOpen->setToolTip(QApplication::translate("omni::ui::MainWindow", "Open a calibration from file", 0));
#endif // QT_NO_TOOLTIP
        actionOpen->setShortcut(QApplication::translate("omni::ui::MainWindow", "Ctrl+O", 0));
        actionSave->setText(QApplication::translate("omni::ui::MainWindow", "Save", 0));
#ifndef QT_NO_TOOLTIP
        actionSave->setToolTip(QApplication::translate("omni::ui::MainWindow", "Save calibration", 0));
#endif // QT_NO_TOOLTIP
        actionSave->setShortcut(QApplication::translate("omni::ui::MainWindow", "Ctrl+S", 0));
        actionScreenSetup->setText(QApplication::translate("omni::ui::MainWindow", "Screen Setup", 0));
#ifndef QT_NO_TOOLTIP
        actionScreenSetup->setToolTip(QApplication::translate("omni::ui::MainWindow", "Setup Projection Screens", 0));
#endif // QT_NO_TOOLTIP
        actionScreenSetup->setShortcut(QApplication::translate("omni::ui::MainWindow", "Ctrl+1", 0));
        actionProjectionSetup->setText(QApplication::translate("omni::ui::MainWindow", "Projection Setup", 0));
#ifndef QT_NO_TOOLTIP
        actionProjectionSetup->setToolTip(QApplication::translate("omni::ui::MainWindow", "Setup Canvas, Mapping and Projectors", 0));
#endif // QT_NO_TOOLTIP
        actionProjectionSetup->setShortcut(QApplication::translate("omni::ui::MainWindow", "Ctrl+2", 0));
        actionBlend->setText(QApplication::translate("omni::ui::MainWindow", "Blend", 0));
#ifndef QT_NO_TOOLTIP
        actionBlend->setToolTip(QApplication::translate("omni::ui::MainWindow", "Edit Blend Masks", 0));
#endif // QT_NO_TOOLTIP
        actionBlend->setShortcut(QApplication::translate("omni::ui::MainWindow", "Ctrl+4", 0));
        actionExport->setText(QApplication::translate("omni::ui::MainWindow", "Export", 0));
#ifndef QT_NO_TOOLTIP
        actionExport->setToolTip(QApplication::translate("omni::ui::MainWindow", "Export Calibration To File", 0));
#endif // QT_NO_TOOLTIP
        actionExport->setShortcut(QApplication::translate("omni::ui::MainWindow", "Ctrl+5", 0));
        actionEditAsNew->setText(QApplication::translate("omni::ui::MainWindow", "EditAsNew", 0));
#ifndef QT_NO_TOOLTIP
        actionEditAsNew->setToolTip(QApplication::translate("omni::ui::MainWindow", "Edit calibration as new", 0));
#endif // QT_NO_TOOLTIP
        actionEditAsNew->setShortcut(QApplication::translate("omni::ui::MainWindow", "Ctrl+Shift+N", 0));
        btnAbout->setText(QApplication::translate("omni::ui::MainWindow", "...", 0));
        btnNew->setText(QApplication::translate("omni::ui::MainWindow", "New", 0));
        btnOpen->setText(QApplication::translate("omni::ui::MainWindow", "Open", 0));
        btnSave->setText(QApplication::translate("omni::ui::MainWindow", "Save", 0));
        btnEditAsNew->setText(QApplication::translate("omni::ui::MainWindow", "Edit As New", 0));
        btnScreenSetup->setText(QApplication::translate("omni::ui::MainWindow", "Screen Setup", 0));
        btnProjectionSetup->setText(QApplication::translate("omni::ui::MainWindow", "Projection Setup", 0));
        btnWarp->setText(QApplication::translate("omni::ui::MainWindow", "Warp", 0));
        btnBlend->setText(QApplication::translate("omni::ui::MainWindow", "Blend", 0));
        btnExport->setText(QApplication::translate("omni::ui::MainWindow", "Play / Export", 0));
        grpCanvas->setProperty("title", QVariant(QApplication::translate("omni::ui::MainWindow", "Canvas", 0)));
        grpProjectors->setTitle(QApplication::translate("omni::ui::MainWindow", "Projectors", 0));
        btnAddTuning->setText(QApplication::translate("omni::ui::MainWindow", "Add", 0));
    } // retranslateUi

};

} // namespace ui
} // namespace omni

namespace omni {
namespace ui {
namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui
} // namespace ui
} // namespace omni

#endif // UI_OMNI_UI_MAINWINDOW_H
